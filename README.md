[fr](README.md) [en](README.en.md)

Archlinux Install on Remote
===========================

Air est un projet libre qui à pour objectif l'installation automatique d'archlinux sur un serveur distant en chroot depuis une autre distribution, par exemple une image rescue debian.

Il est composé de plusieurs scripts shell, playbook et roles ansible.

Il genere aussi automatiquement l'inventaire et la configuration ansible, creer des clefs ssh specifiquement pour cette usage. Et permet ensuite d'installer divers services assez commun via les roles ansible du projet.

C'est un projet en cours, meme si certaine fonctionnalité marche déjà bien.
Il reste en particulier pas mal de travail a faire pour les services "haut niveau" comme nextcloud et gitlab.

Il cherche à rester KISS, comme Archlinux.

Depots Git
----------

- Doc

https://git.laquadrature.net/sympathisant-es/archlinux-install-on-remote/doc

Un jour il y'aura peut-etre ici une doc complete sur le projet.

- Init

https://git.laquadrature.net/sympathisant-es/archlinux-install-on-remote/init

C'est le depot qui creer l'inventaire ansible et install Archlinux en chroot sur le serveur distant.

- Install

https://git.laquadrature.net/sympathisant-es/archlinux-install-on-remote/install

C'est le depot qui sert a installer les differents services, comme docker, mais aussi a faire la configuration basique du systeme, comme par exemple l'ajout de disque.

Il peux aussi servir pour lancer d'autre role ansible comme pour l'installation de mailcow qui utilise le role officiel/

- Ansible

https://git.laquadrature.net/sympathisant-es/archlinux-install-on-remote/air-ansible

Il y'a un depot par role Ansible, certain sont fini, d'autre encore en cours de dev.

Contribution
------------

Toutes les contributions sont les bienvenues. Avis, Questions, Relecture, Ecriture, Merge Request ...

Si vous pouvez, venez en discutez sur Matrix avant https://matrix.to/#/#fr-archlinux:matrix.org ou sur le forum archlinux fr: https://forums.archlinux.fr/viewtopic.php?t=22764

Remerciement
------------
- Merci à la quadrature d'heberger le projet sur leur gitlab.
- Merci à la communauté archlinux.
- Merci au logiciel Libre.

Todo
----
- Faire un logo
- Traduire cette presentation en anglais
- Ecrire la doc
- Finir les roles en cours
- Etre gentil

Projet Similaire
----------------

- https://github.com/antoinemartin/archlinux-ansible-install
