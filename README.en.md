[fr](README.md) [en](README.en.md)

Archlinux Install on Remote
===========================

A.I.R. is an opensource project aiming at the automatic install of archlinux on a remote server from within a *chroot* (from another distro), for example, a *debian* rescue image.

It is made of multiple shell scripts, and ansible playbooks and roles.


The *ansible* inventory and configuration are autmatically generated, as well as specific *ssh* keys.
Most common services can also be installed with ansible roles of the project.

It is a work in progress, but some features are already fonctional.

Most of the remaining work is on the "high level" services like *nextcloud* and *gitlab*.

This project tries to follow the KISS philosophy of Archlinux.

Git repositories
----------------

- Doc

https://git.laquadrature.net/sympathisant-es/archlinux-install-on-remote/doc

This is expected to host the complete documentation of the project, in the near future.

- Init

https://git.laquadrature.net/sympathisant-es/archlinux-install-on-remote/init

This is the repository used to both create the ansible invetory and install archlinux on the remote server.

- Install

https://git.laquadrature.net/sympathisant-es/archlinux-install-on-remote/install

This is the repository used to install the other services like *docker*, but it also does the bsaic configuration of the system, like adding the disk.

It can also be used to launch other ansible role like *mailcow* which uses the official role.

- Ansible

https://git.laquadrature.net/sympathisant-es/archlinux-install-on-remote/air-ansible

There is a repository for each ansible role. Some are finished, others not yet.

Contribution
------------

Any contribution is welcome. Questions, opinions, review, pull request ...

Please, come discuss here: https://bbs.archlinux.org/viewtopic.php?id=284084

Thanks to
---------
- LQDN (La Quadatrure du Net) for hosting this project on their gitlab
- to the arhclinux community
- to open source

Todo
----
- Make a logo
- Write the documentation
- Finish the roles
- Be kind

Similare Project
----------------

- https://github.com/antoinemartin/archlinux-ansible-install
